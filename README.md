# Teste Madeinweb - iOS - Silvano Junior

## Instruções para compilar e executar o projeto
- Faça um clone da branch Master em sua máquina
- Abra o terminal e na raiz do projeto, rode o comando pod install
- Aguarde até todas as dependências serem instaladas
- Após finalizar, abra o projeto através do arquivo .xcworkspace
- Build e Rode o projeto em seu device ou simulador

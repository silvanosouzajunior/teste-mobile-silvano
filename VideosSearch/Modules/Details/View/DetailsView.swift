//
//  DetailsView.swift
//  VideosSearch
//
//  Created by Silvano Junior on 09/01/19.
//  Copyright © 2019 Silvano. All rights reserved.
//

import UIKit

class DetailsView: UIView {

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var viewsLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var coverImageView: UIImageView!
    
    var viewModel: DetailsViewModel? {
        didSet {
            if let item = viewModel?.item {
                update(with: item)
            }
        }
    }
    
    func update(with item: ItemDetails) {
        if let urlString = item.snippet?.thumbnail?.defaultThumbnail?.url, let url = URL(string: urlString) {
            coverImageView.af_setImage(withURL: url)
        }
        
        titleLabel.text = item.snippet?.title ?? ""
        viewsLabel.text = "\(item.statistics?.viewCount ?? "") visualizações"
        descriptionLabel.text = item.snippet?.description ?? ""
    }

    @IBAction func backButtonTapped(_ sender: UIButton) {
        viewModel?.back()
    }
}

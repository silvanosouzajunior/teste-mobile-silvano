//
//  DetailsViewController.swift
//  VideosSearch
//
//  Created by Silvano Junior on 09/01/19.
//  Copyright © 2019 Silvano. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    @IBOutlet var detailsView: DetailsView!
    
    var viewModel: DetailsViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        detailsView.viewModel = viewModel
    }
}

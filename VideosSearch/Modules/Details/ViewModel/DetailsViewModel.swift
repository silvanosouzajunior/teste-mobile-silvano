//
//  DetailsViewModel.swift
//  VideosSearch
//
//  Created by Silvano Junior on 09/01/19.
//  Copyright © 2019 Silvano. All rights reserved.
//

import UIKit

protocol DetailsViewModelViewDelegate: class {
}

protocol DetailsViewModelDelegate: class {
    func back()
}

class DetailsViewModel: NSObject {
    
    weak var coordinatorDelegate: DetailsViewModelDelegate?
    weak var viewDelegate: DetailsViewModelViewDelegate?
    
    var item: ItemDetails
    
    init(item: ItemDetails) {
        self.item = item
    }
    
    func back() {
        coordinatorDelegate?.back()
    }
}

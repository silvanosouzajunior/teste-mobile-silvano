//
//  DetailsCoordinator.swift
//  VideosSearch
//
//  Created by Silvano Junior on 09/01/19.
//  Copyright © 2019 Silvano. All rights reserved.
//

import UIKit

class DetailsCoordinator: NSObject {
    
    var navigationController: UINavigationController
    var currentCoordinator: DetailsCoordinator?
    var item: ItemDetails
    
    init(navigationController: UINavigationController, item: ItemDetails) {
        self.navigationController = navigationController
        self.item = item
    }
    
    func start() {
        let detailsViewController = instantiateDetailsViewController()

        let viewModel = DetailsViewModel(item: item)
        viewModel.coordinatorDelegate = self
        
        detailsViewController.viewModel = viewModel
        
        setupNavigationController()
        
        self.navigationController.present(detailsViewController, animated: true, completion: nil)
        self.currentCoordinator = self
    }
    
    private func setupNavigationController() {
        self.navigationController.isNavigationBarHidden = true
    }
    
    private func instantiateDetailsViewController() -> DetailsViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let detailsViewController = storyboard.instantiateViewController(withIdentifier: "VideoDetails") as! DetailsViewController
        return detailsViewController
    }
}

extension DetailsCoordinator: DetailsViewModelDelegate {
    func back() {
        self.navigationController.dismiss(animated: true, completion: nil)
    }
}

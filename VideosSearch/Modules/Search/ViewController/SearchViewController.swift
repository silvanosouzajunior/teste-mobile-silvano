//
//  SearchViewController.swift
//  VideosSearch
//
//  Created by Silvano Junior on 08/01/19.
//  Copyright © 2019 Silvano. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {
    
    @IBOutlet var searchView: SearchView!
    
    var viewModel: SearchViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        searchView.viewModel = viewModel
    }
}

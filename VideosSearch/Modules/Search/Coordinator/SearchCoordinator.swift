//
//  SearchCoordinator.swift
//  VideosSearch
//
//  Created by Silvano Junior on 08/01/19.
//  Copyright © 2019 Silvano. All rights reserved.
//

import UIKit

class SearchCoordinator: NSObject {
    
    var navigationController: UINavigationController
    var currentCoordinator: SearchCoordinator?
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let viewModel = SearchViewModel()
        viewModel.coordinatorDelegate = self
        
        let searchViewController = instantiateSearchViewController()
        searchViewController.viewModel = viewModel
        
        setupNavigationController()
        
        self.navigationController.pushViewController(searchViewController, animated: true)
        self.currentCoordinator = self
    }
    
    func showItemDetails(item: ItemDetails) {
        let detailsCoordinator = DetailsCoordinator(navigationController: navigationController, item: item)
        detailsCoordinator.start()
    }
    
    private func setupNavigationController() {
        self.navigationController.isNavigationBarHidden = true
    }
    
    private func instantiateSearchViewController() -> SearchViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let searchViewController = storyboard.instantiateViewController(withIdentifier: "SearchVideos") as! SearchViewController
        return searchViewController
    }
}

extension SearchCoordinator: SearchViewModelDelegate {
    func showDetails(with item: ItemDetails) {
        showItemDetails(item: item)
    }
}

//
//  SearchViewModel.swift
//  VideosSearch
//
//  Created by Silvano Junior on 08/01/19.
//  Copyright © 2019 Silvano. All rights reserved.
//

import UIKit

protocol SearchViewModelViewDelegate: class {
    func reloadTableView()
}

protocol SearchViewModelDelegate: class {
    func showDetails(with item: ItemDetails)
}

class SearchViewModel: NSObject {
    
    weak var coordinatorDelegate: SearchViewModelDelegate?
    weak var viewDelegate: SearchViewModelViewDelegate?
    
    let videosAPI = VideosAPI()
    var items = [Item]()
    
    func search(with term:String) {
        videosAPI.search(with: term, completion: {
            items in
            
            self.items = items ?? []
            self.viewDelegate?.reloadTableView()
        })
    }
    
    func getVideoDetails(with id:String) {
        videosAPI.getDetails(with: id, completion: {
            item in
            
            if let item = item {
                self.coordinatorDelegate?.showDetails(with: item)
            }
        })
    }
}

extension SearchViewModel: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VideoCell", for: indexPath) as! VideoTableViewCell
        
        let item = items[indexPath.row]
        
        if let video = item.snippet {
            cell.configure(with: video)
        }
        
        return cell
    }
}

extension SearchViewModel: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        
        if let id = item.id?.videoId {
            getVideoDetails(with: id)
        }
    }
}

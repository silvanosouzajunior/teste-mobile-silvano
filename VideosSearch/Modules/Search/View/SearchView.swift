//
//  SearchView.swift
//  VideosSearch
//
//  Created by Silvano Junior on 08/01/19.
//  Copyright © 2019 Silvano. All rights reserved.
//

import UIKit

class SearchView: UIView {
    
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var buttonTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var textFieldLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var textFieldTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var textFieldTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var logoWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var logoTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var searchTextField: UITextField! {
        didSet {
            searchTextField.layer.borderWidth = 1.0
            searchTextField.layer.borderColor = UIColor.gray.cgColor
        }
    }
    
    var viewModel: SearchViewModel? {
        didSet {
            if tableView != nil {
                tableView.dataSource = viewModel
                tableView.delegate = viewModel
            }
            viewModel?.viewDelegate = self
        }
    }
    
    private func showTableViewResults() {
        logoWidthConstraint.constant = 115
        logoTopConstraint.constant = 40
        textFieldTopConstraint.constant = 18
        textFieldLeadingConstraint.constant = 13
        textFieldTrailingConstraint.constant = 113
        buttonTopConstraint.constant = -20.5
        buttonTrailingConstraint.constant = 13
        buttonLeadingConstraint.constant = UIScreen.main.bounds.size.width - 120
        
        UIView.animate(withDuration: 1.0, animations: {
            self.tableView.isHidden = false
            self.layoutIfNeeded()
        })
    }
    
    private func hideTableViewResults() {
        logoWidthConstraint.constant = 265
        logoTopConstraint.constant = 164
        textFieldTopConstraint.constant = 80
        textFieldLeadingConstraint.constant = 30
        textFieldTrailingConstraint.constant = 30
        buttonTopConstraint.constant = 60
        buttonTrailingConstraint.constant = 30
        buttonLeadingConstraint.constant = 30
        
        UIView.animate(withDuration: 1.0, animations: {
            self.tableView.isHidden = true
            self.layoutIfNeeded()
        })
    }
    
    @IBAction func logoImageViewTapped(_ sender: UITapGestureRecognizer) {
        hideTableViewResults()
    }
    
    @IBAction func searchButtonTapped(_ sender: UIButton) {
        showTableViewResults()
        viewModel?.search(with: searchTextField.text ?? "")
    }
}

extension SearchView: SearchViewModelViewDelegate {
    func reloadTableView() {
        tableView.reloadData()
    }
}

//
//  VideoTableViewCell.swift
//  VideosSearch
//
//  Created by Silvano Junior on 08/01/19.
//  Copyright © 2019 Silvano. All rights reserved.
//

import UIKit
import AlamofireImage

class VideoTableViewCell: UITableViewCell {

    @IBOutlet weak var videoDescriptionLabel: UILabel!
    @IBOutlet weak var videoTitleLabel: UILabel!
    @IBOutlet weak var videoImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configure(with video: Video) {
        if let urlString = video.thumbnail?.defaultThumbnail?.url, let url = URL(string: urlString) {
            videoImageView.af_setImage(withURL: url)
        }
        
        videoTitleLabel.text = video.title ?? ""
        videoDescriptionLabel.text = video.description ?? ""
    }
}

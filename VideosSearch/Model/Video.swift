//
//  Video.swift
//  VideosSearch
//
//  Created by Silvano Junior on 08/01/19.
//  Copyright © 2019 Silvano. All rights reserved.
//

import Foundation

struct Item: Codable {
    
    enum CodingKeys: String, CodingKey {
        case snippet = "snippet"
        case id = "id"
    }
    
    var snippet: Video?
    var id: VideoId?
}

struct ItemDetails: Codable {
    
    enum CodingKeys: String, CodingKey {
        case snippet = "snippet"
        case id = "id"
        case statistics = "statistics"
    }
    
    var snippet: Video?
    var id: String?
    var statistics: Statistics?
}

struct VideoId: Codable {
    
    enum CodingKeys: String, CodingKey {
        case videoId = "videoId"
    }
    
    var videoId: String?
}

struct Video: Codable {
    
    enum CodingKeys: String, CodingKey {
        case title = "title"
        case description = "description"
        case channelTitle = "channelTitle"
        case thumbnail = "thumbnails"
    }
    
    var title: String?
    var description: String?
    var channelTitle: String?
    var thumbnail: Thumbnail?
}

struct Thumbnail: Codable {
    enum CodingKeys: String, CodingKey {
        case defaultThumbnail = "default"
    }
    
    var defaultThumbnail: DefaultThumbnail?
}

struct Statistics: Codable {
    enum CodingKeys: String, CodingKey {
        case viewCount = "viewCount"
    }
    
    var viewCount: String?
}

struct DefaultThumbnail: Codable {
    enum CodingKeys: String, CodingKey {
        case url = "url"
        case width = "width"
        case height = "height"
    }
    
    var url: String?
    var width: Int?
    var height: Int?
}

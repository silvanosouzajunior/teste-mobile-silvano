//
//  VideosList.swift
//  VideosSearch
//
//  Created by Silvano Junior on 08/01/19.
//  Copyright © 2019 Silvano. All rights reserved.
//

struct VideosList: Codable {
    
    enum CodingKeys: String, CodingKey {
        case items = "items"
    }
    
    var items: [Item]?
}

struct VideoDetails: Codable {
    
    enum CodingKeys: String, CodingKey {
        case items = "items"
    }
    
    var items: [ItemDetails]?
}

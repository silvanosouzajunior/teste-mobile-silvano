//
//  VideosAPI.swift
//  VideosSearch
//
//  Created by Silvano Junior on 08/01/19.
//  Copyright © 2019 Silvano. All rights reserved.
//

import Foundation
import Alamofire

class VideosAPI {
    
    let apiKey = "AIzaSyBWLmZxgWmMYd-mFqsb-CO7OpktSE8-SYE"
    
    func search(with term: String, completion: @escaping ([Item]?) -> ()) {
        let youtubeUrl = "https://www.googleapis.com/youtube/v3/search?part=id,snippet&q=\(term)&key=\(apiKey)"
        let urlString = youtubeUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""

        guard let url = URL(string: urlString) else { return }
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).validate()
            .responseData { (response) in
                switch response.result {
                case .success(let data):
                    do {
                        let videosList = try JSONDecoder().decode(VideosList.self, from: data)
                        completion(videosList.items ?? [])
                    } catch {
                        return completion(nil)
                    }
                case .failure(_):
                    return completion(nil)
                }
        }
    }
    
    func getDetails(with id: String, completion: @escaping (ItemDetails?) -> ()) {
        let youtubeUrl = "https://www.googleapis.com/youtube/v3/videos?id=\(id)&part=snippet,statistics&key=\(apiKey)"
        let urlString = youtubeUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        
        guard let url = URL(string: urlString) else { return }
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).validate()
            .responseData { (response) in
                switch response.result {
                case .success(let data):
                    do {
                        let videoDetails = try JSONDecoder().decode(VideoDetails.self, from: data)
                        completion(videoDetails.items?.first)
                    } catch {
                        return completion(nil)
                    }
                case .failure(_):
                    return completion(nil)
                }
        }
    }
}
